package com.example.myirancell.viewmodel

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.util.Log
import android.widget.Toast
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.myirancell.models.AmazingItem
import com.example.myirancell.models.AmazingSuperMarketItem
import com.example.myirancell.remote.NetworkResult
import com.example.myirancell.repository.HomeRepository
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val repository: HomeRepository) : ViewModel() {
    val superMarketItems =
        MutableStateFlow<NetworkResult<List<AmazingSuperMarketItem>>>(NetworkResult.Loading())

    lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    var myLocation = MutableStateFlow<Location?>(Location(""))

    suspend fun getAllDataFromServer() {

        viewModelScope.launch() {

            launch {
                repository.getSuperMarketItems().let { networkResult ->
                    networkResult.data!!.asFlow().filter { it.discountPercent <= 10 }.toList().let {
                        superMarketItems.emit(networkResult)
                        superMarketItems.value.data = it

                    }
                }
            }

        }
    }

    fun fetchLocation(context: Context, activity: Activity) {

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context)
        val task = fusedLocationProviderClient.lastLocation
        if (ActivityCompat.checkSelfPermission(
                context,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                context,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                activity,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                101
            )

        }
        task.addOnSuccessListener {
            if (it != null) {
                //  Toast.makeText(context,it.latitude.toString(),Toast.LENGTH_LONG).show()
                myLocation.value = it
            }
        }
        return
    }

}