package com.example.myirancell.remote

import com.example.myirancell.models.AmazingItem
import com.example.myirancell.models.AmazingSuperMarketItem
import com.example.myirancell.models.ResponseResult
import retrofit2.Response
import retrofit2.http.GET

interface ApiInterface {

    @GET("getSuperMarketAmazingProducts")
    suspend fun getSuperMarketItems(): Response<ResponseResult<List<AmazingSuperMarketItem>>>


}