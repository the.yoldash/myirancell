package com.example.myirancell.remote
sealed class NetworkResult<T>(
    var data: T? = null,
    var message: String? = null
) {
    class Success<T>(message: String, data: T) : NetworkResult<T>(data, message)
    class Error<T>(message: String, data: T? = null) : NetworkResult<T>(data, message)
    class Loading<T> : NetworkResult<T>()
}