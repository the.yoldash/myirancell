package com.example.myirancell.ui.screen.home

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.SideEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.myirancell.viewmodel.HomeViewModel
import com.google.accompanist.systemuicontroller.rememberSystemUiController

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun HomeScreen(viewModel: HomeViewModel = hiltViewModel()) {
    val systemUiController = rememberSystemUiController()
    SideEffect {
        systemUiController.setStatusBarColor(
            color = Color(0xFFffbe1a),
            darkIcons = false
        )
    }
    LaunchedEffect(true) {
        viewModel.getAllDataFromServer()
    }
    Column(modifier = Modifier
        .fillMaxSize()
        .background(Color(0xFFe7e7e7))) {
        Box(
            modifier = Modifier
                .clip(shape = RoundedCornerShape(bottomEnd = 20.dp, bottomStart = 20.dp))
                .background(Color(0xFFf8b300))
                .fillMaxWidth()
                .height(330.dp)
        ) {
            TopAppBarSection()
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .align(Alignment.BottomCenter)
                    .height(260.dp)
                    .clip(shape = RoundedCornerShape(20.dp))
                    .background(Color.White)
            ) {
                StatusSection()
            }
        }
        Spacer(modifier = Modifier.height(10.dp))

       GameBarSection()
        AmazingSection()
    }
}