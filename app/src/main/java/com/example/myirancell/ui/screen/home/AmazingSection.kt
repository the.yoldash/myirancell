package com.example.myirancell.ui.screen.home

import android.app.Activity
import android.location.Location
import android.os.Build

import android.util.Log
import androidx.annotation.RequiresApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Tab
import androidx.compose.material3.TabRow
import androidx.compose.material3.TabRowDefaults.tabIndicatorOffset
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import coil.compose.rememberAsyncImagePainter
import com.example.myirancell.R
import com.example.myirancell.models.AmazingSuperMarketItem
import com.example.myirancell.remote.NetworkResult
import com.example.myirancell.viewmodel.HomeViewModel
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.compose.GoogleMap
import com.google.maps.android.compose.Marker
import com.google.maps.android.compose.MarkerState
import com.google.maps.android.compose.rememberCameraPositionState
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest

@OptIn(ExperimentalLayoutApi::class)
@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun AmazingSection(
    viewModel: HomeViewModel = hiltViewModel()
) {
    var selectedTabIndex by remember { mutableStateOf(0) } // 1.
    val tabTitles =
        listOf("داغ", "نقشه گوگل ", "مالی")
    /*val amazingItems =
        listOf(
            AmazingItem(R.drawable.snapp,"اسنپ"),
            AmazingItem(R.drawable.snap_food,"اسنپ فود"),
            AmazingItem(R.drawable.toranj,"ترنج"),
            AmazingItem(R.drawable.torob,"ترب"),
            AmazingItem(R.drawable.robika,"روبیکا"),
            AmazingItem(R.drawable.jajiga,"جاجیگا"),
            AmazingItem(R.drawable.jabama,"جاباما"),
            AmazingItem(R.drawable.ita,"ایتا"),
            AmazingItem(R.drawable.mofid,"مفید"),
            AmazingItem(R.drawable.alibaba,"علی بابا"),
            AmazingItem(R.drawable.khanomi,"خانومی"),
            AmazingItem(R.drawable.bazar,"بازار"),

        )*/
    val icons =
        listOf(
            painterResource(id = R.drawable.fire),
            painterResource(id = R.drawable.medal),
            painterResource(id = R.drawable.money_bag)
        )
    var counterState = 0
    val currentCartItemsCount = remember {
        mutableStateOf(0)
    }
    val nextCartItemsCount = remember {
        mutableStateOf(0)
    }

    var list by remember {
        mutableStateOf<List<AmazingSuperMarketItem>>(emptyList())
    }
    var loading by remember {
        mutableStateOf(false)
    }
    LaunchedEffect(Dispatchers.Main) {
        loading = true
        viewModel.superMarketItems.collectLatest { result ->
            when (result) {
                is NetworkResult.Success -> {
                    result.data?.let {
                        list = it
                    }
                    loading = false
                }

                is NetworkResult.Error -> {
                    loading = false
                    Log.d("2121", "InitAmazingItems error:${result.message} ")
                    // show error message
                }

                is NetworkResult.Loading -> {
                    loading = true
                }

                else -> {}
            }
        }
    }


    Spacer(modifier = Modifier.height(10.dp))

    Column(modifier = Modifier.clip(shape = RoundedCornerShape(15.dp))) {
        TabRow(

            contentColor = Color.Black,
            selectedTabIndex = selectedTabIndex,
            indicator = { line ->
                Box(
                    modifier = Modifier
                        .tabIndicatorOffset(line[selectedTabIndex])
                        .height(3.dp)
                        .background(color = Color(0xFFffbe1a)),
                )
            }

        ) {
            tabTitles.forEachIndexed { index, title ->
                Tab(
                    selected = selectedTabIndex == index,
                    selectedContentColor = Color.Black,
                    unselectedContentColor = Color.Gray,
                    onClick = { selectedTabIndex = index },
                    text = {
                        Row() {
                            Text(
                                text = title,
                                fontSize = 10.sp,
                                fontWeight = FontWeight.Normal,
                                overflow = TextOverflow.Ellipsis,
                                fontFamily = FontFamily(Font(R.font.sans))
                            )
                            Image(
                                painter = (icons.get(index)),
                                contentDescription = "",
                                modifier = Modifier.size(15.dp)
                            )
                        }
                    },
                )
            }

        }
        when (selectedTabIndex) {
            0 -> {
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(400.dp)
                        .background(Color.White)
                ) {
                    FlowRow(
                        maxItemsInEachRow = 4,
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(290.dp)
                            .padding(8.dp),
                        horizontalArrangement = Arrangement.SpaceEvenly
                    ) {
                        for (item in list) {
                            Column(
                                horizontalAlignment = Alignment.CenterHorizontally,
                                modifier = Modifier
                                    .size(90.dp)
                                    .padding(10.dp)
                            ) {
                                Image(
                                    painter = rememberAsyncImagePainter(item.image),
                                    contentDescription = "",
                                    modifier = Modifier
                                        .size(55.dp)
                                        .clickable { })
                                Text(
                                    text = "${item.discountPercent} %",
                                    fontSize = 12.sp,
                                    fontFamily = FontFamily(
                                        Font(R.font.sans)
                                    ),
                                    color = Color.Black,
                                    fontWeight = FontWeight.Bold
                                )
                            }
                        }
                    }
                }
            }

            1 -> {
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(400.dp)
                        .background(Color.White),
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    var context = LocalContext.current
                    var lat by remember {
                        mutableStateOf(0.0)
                    }
                    var long by remember {
                        mutableStateOf(0.0)
                    }
                    LaunchedEffect(key1 = true) {
                        viewModel.fetchLocation(context = context, activity = Activity())
                        viewModel.myLocation.collectLatest {
                            if (it != null) {
                                lat = it.latitude
                                long = it.longitude
                            }
                        }
                    }

                    if (lat > 0 && long > 0) {
                        val cameraPositionState = rememberCameraPositionState {
                            position = CameraPosition.fromLatLngZoom(LatLng(lat, long), 8f)
                        }
                        GoogleMap(
                            modifier = Modifier
                                .fillMaxWidth()
                                .height(400.dp),
                            cameraPositionState = cameraPositionState,
                        ) {
                            Marker(
                                state = MarkerState(LatLng(lat, long)),
                                title = "map",
                                snippet = "marker in here",
                            )
                        }
                    }
                }
            }

            2 -> {
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(400.dp)
                        .background(Color.White),
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Text(
                        text = "تب مالی",
                        fontFamily = FontFamily(Font(R.font.sans)),
                        fontSize = 12.sp
                    )
                }
            }
        }
    }
}
