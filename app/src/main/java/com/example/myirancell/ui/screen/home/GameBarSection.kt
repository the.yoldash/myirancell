package com.example.myirancell.ui.screen.home

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.myirancell.R

@Composable
fun GameBarSection() {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .height(50.dp)
            .clip(RoundedCornerShape(15.dp))
            .background(
                Color.White
            ),
        verticalAlignment = Alignment.CenterVertically

    ) {
        Image(painter = painterResource(id = R.drawable.wallet), contentDescription ="", modifier = Modifier
            .padding(start = 16.dp)
            .size(25.dp)

        )
        Text(text = "رانسل رو بازی کنید و جایزه بگیرید ", fontFamily = FontFamily(Font(R.font.sans)), color = Color.Black, fontSize = 12.sp, fontWeight = FontWeight.Black, modifier = Modifier.padding(start = 8.dp))
        Text(text = "!", fontFamily = FontFamily(Font(R.font.sans)), color = Color(0xFF6aeba8), fontSize = 12.sp, fontWeight = FontWeight.Black, modifier = Modifier.padding(start = 0.dp))
    }
}