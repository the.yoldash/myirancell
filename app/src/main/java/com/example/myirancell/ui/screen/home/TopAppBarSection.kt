package com.example.myirancell.ui.screen.home

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.myirancell.R
import com.example.myirancell.utils.Helper

@Composable
fun TopAppBarSection() {
    Row(
        modifier = Modifier
            .padding( horizontal = 16.dp)
            .fillMaxWidth()
            .height(70.dp),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {

        Row(
            modifier = Modifier
                .clip(shape = RoundedCornerShape(10.dp))
                .background(Color(0xFFffbe1a))
                .width(220.dp)
                .height(45.dp),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {

            Row(modifier = Modifier.fillMaxHeight()) {
                Icon(
                    painter = painterResource(id = R.drawable.irancell),
                    contentDescription = "",
                    modifier = Modifier
                        .size(40.dp)
                )
                Column(
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally,
                    modifier = Modifier
                        .fillMaxHeight()
                        .padding(start = 8.dp)
                ) {
                    Text(text = "mohammad935", fontSize = 11.sp, fontWeight = FontWeight.Black)
                    Text(
                        text = Helper.EnglishToPersian("989353639630"),
                        fontSize = 15.sp,
                        fontWeight = FontWeight.Bold
                    )
                }

            }
            Icon(
                painter = painterResource(id = R.drawable.arrow),
                contentDescription = "",
                modifier = Modifier
                    .size(25.dp)
                    .padding(end = 4.dp)
            )
        }
        IconButton(
            modifier = Modifier
                .clip(shape = RoundedCornerShape(10.dp))
                .size(45.dp)
                .background(Color(0xFFffbe1a)),
            onClick = { /*TODO*/ }
        ) {
            Icon(
                painter = painterResource(id = R.drawable.bell),
                contentDescription = "",
                Modifier.size(20.dp)
            )
        }
    }
}