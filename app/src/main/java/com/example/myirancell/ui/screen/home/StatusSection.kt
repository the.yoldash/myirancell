package com.example.myirancell.ui.screen.home

import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.myirancell.R
import com.example.myirancell.utils.Helper

@Composable
fun StatusSection() {

        Column() {


            Row(
                Modifier
                    .fillMaxWidth()
                    .height(130.dp),
                horizontalArrangement = Arrangement.SpaceEvenly,
                verticalAlignment = Alignment.CenterVertically
            ) {

                Column(
                    modifier = Modifier
                        .clip(shape = RoundedCornerShape(10.dp))
                        .border(
                            width = 1.dp,
                            color = Color.LightGray,
                            shape = RoundedCornerShape(10.dp)
                        )
                        .size(width = 170.dp, height = 110.dp)
                        .padding(8.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Text(
                        text = "کارکرد کل",
                        color = Color.LightGray,
                        fontSize = 12.sp,
                        fontFamily = FontFamily(
                            Font(R.font.sans)
                        ),
                        fontWeight = FontWeight.Black
                    )
                    Row() {
                        Text(
                            text = Helper.EnglishToPersian("422,040"),
                            color = Color.Black,
                            fontSize = 12.sp,
                            fontFamily = FontFamily(
                                Font(R.font.sans)
                            )
                        )
                        Text(
                            text = Helper.EnglishToPersian(" ریال "),
                            color = Color.Black,
                            fontSize = 12.sp,
                            fontFamily = FontFamily(
                                Font(R.font.sans)
                            )
                        )

                    }
                    Spacer(modifier = Modifier.height(10.dp))

                    Row(
                        horizontalArrangement = Arrangement.SpaceEvenly,
                        modifier = Modifier.fillMaxWidth()
                    ) {

                        Column(

                            modifier = Modifier
                                .clip(shape = RoundedCornerShape(30.dp))
                                .size(width = 100.dp, height = 32.dp)
                                .clickable { }
                                .background(Color(0xFFf8b300)),
                            horizontalAlignment = Alignment.CenterHorizontally,
                            verticalArrangement = Arrangement.Center
                        ) {
                            Text(
                                text = "پرداخت میان دوره",
                                color = Color.Black,
                                fontSize = 11.sp,
                                fontFamily = FontFamily(
                                    Font(R.font.sans)
                                ),

                                )
                        }
                        IconButton(
                            onClick = { /*TODO*/ },
                            modifier = Modifier
                                .clip(shape = CircleShape)
                                .size(30.dp)
                                .background(
                                    Color(0xFFf4f4f6)
                                )
                        ) {
                            Icon(
                                painter = painterResource(id = R.drawable.arrow),
                                contentDescription = "",
                                modifier = Modifier
                                    .size(15.dp)
                            )
                        }

                    }
                }
                Column(
                    modifier = Modifier
                        .clip(shape = RoundedCornerShape(10.dp))
                        .border(
                            width = 1.dp,
                            color = Color.LightGray,
                            shape = RoundedCornerShape(10.dp)
                        )
                        .size(width = 170.dp, height = 110.dp)
                        .padding(8.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Text(
                        text = "موجودی کل جیبت",
                        color = Color.LightGray,
                        fontSize = 12.sp,
                        fontFamily = FontFamily(
                            Font(R.font.sans)
                        ),
                        fontWeight = FontWeight.Black
                    )
                    Row() {

                        Text(
                            text = Helper.EnglishToPersian("65,000"),
                            color = Color.Black,
                            fontSize = 12.sp,
                            fontFamily = FontFamily(
                                Font(R.font.sans)
                            )
                        )
                        Text(
                            text = Helper.EnglishToPersian(" ریال "),
                            color = Color.Black,
                            fontSize = 12.sp,
                            fontFamily = FontFamily(
                                Font(R.font.sans)
                            )
                        )                    }
                    Spacer(modifier = Modifier.height(10.dp))
                    Column(
                        modifier = Modifier
                            .clip(RoundedCornerShape(30.dp))
                            .size(width = 120.dp, height = 32.dp)
                            .background(Color(0xFF6aeba8)),
verticalArrangement = Arrangement.Center,
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Text(
                            text = "مدیریت جیبت",
                            color = Color.Black,
                            fontSize = 11.sp,
                            fontFamily = FontFamily(
                                Font(R.font.sans)
                            ),
                        )
                    }
                }
            }

            Row(
                Modifier
                    .fillMaxWidth()
                    .height(130.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.arrow),
                    contentDescription = "",
                    modifier = Modifier
                        .size(20.dp)
                        .rotate(270f)
                )



                Column(
                    modifier = Modifier
                        .fillMaxHeight()
                        .width(85.dp)
                        .background(Color.White),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Spacer(modifier = Modifier.height(5.dp))
                    Icon(
                        painter = painterResource(id = R.drawable.people),
                        contentDescription = "",
                        modifier = Modifier
                            .clip(
                                CircleShape
                            )
                            .size(55.dp)
                            .background(Color(0xFFf8b300))
                            .padding(8.dp)
                    )
                    Spacer(modifier = Modifier.height(20.dp))

                    Column(
                        modifier = Modifier
                            .clip(shape = RoundedCornerShape(10.dp))
                            .size(width = 80.dp, height = 30.dp)
                            .clickable { }
                            .background(Color(0xFFf8b300)),
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.Center
                    ) {
                        Text(
                            text = "خرید بسته چندکاربره",
                            color = Color.Black,
                            fontSize = 8.sp,
                            fontFamily = FontFamily(
                                Font(R.font.sans)
                            ),

                            )

                    }
                }
                Column(
                    modifier = Modifier
                        .fillMaxHeight()
                        .width(85.dp)
                        .background(Color.White),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Spacer(modifier = Modifier.height(5.dp))
                    Icon(
                        painter = painterResource(id = R.drawable.percent),
                        contentDescription = "",
                        modifier = Modifier
                            .clip(
                                CircleShape
                            )
                            .size(55.dp)
                            .background(Color(0xFFf8b300))
                            .padding(20.dp)
                    )
                    Spacer(modifier = Modifier.height(20.dp))

                    Column(
                        modifier = Modifier
                            .clip(shape = RoundedCornerShape(10.dp))
                            .size(width = 80.dp, height = 30.dp)
                            .clickable { }
                            .background(Color(0xFFf8b300)),
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.Center
                    ) {
                        Text(
                            text = "خرید بسته پیشنهادی",
                            color = Color.Black,
                            fontSize = 8.sp,
                            fontFamily = FontFamily(
                                Font(R.font.sans)
                            ),

                            )

                    }
                }
                Column(
                    modifier = Modifier
                        .fillMaxHeight()
                        .width(85.dp)
                        .background(Color.White),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Spacer(modifier = Modifier.height(5.dp))
                    Icon(
                        painter = painterResource(id = R.drawable.phone_call),
                        contentDescription = "",
                        modifier = Modifier
                            .clip(
                                CircleShape
                            )
                            .size(55.dp)
                            .background(Color(0xFFf8b300))
                            .padding(15.dp)
                    )
                    Spacer(modifier = Modifier.height(20.dp))

                    Column(
                        modifier = Modifier
                            .clip(shape = RoundedCornerShape(10.dp))
                            .size(width = 80.dp, height = 30.dp)
                            .clickable { }
                            .background(Color(0xFFf8b300)),
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.Center
                    ) {
                        Text(
                            text = "خرید بسته مکالمه",
                            color = Color.Black,
                            fontSize = 8.sp,
                            fontFamily = FontFamily(
                                Font(R.font.sans)
                            ),

                            )
                    }

                }
                Column(
                    modifier = Modifier
                        .fillMaxHeight()
                        .width(85.dp)
                        .background(Color.White),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {

                    Box(modifier = Modifier.size(70.dp), contentAlignment = Alignment.Center) {
                        Text(
                            text = "446.3مگ",
                            fontSize = 9.sp,
                            fontFamily = FontFamily(Font(R.font.sans))
                        )
                        val infiniteTransition = rememberInfiniteTransition()

                        CircularProgressIndicator(
                            modifier = Modifier.then(Modifier.size(60.dp)),
                            color = Color(0xFF6aeba8),
                            progress = 0.9f,
                        )
                    }
                    Spacer(modifier = Modifier.height(10.dp))
                    Column(

                        modifier = Modifier
                            .clip(shape = RoundedCornerShape(10.dp))
                            .size(width = 80.dp, height = 30.dp)
                            .clickable { }
                            .background(Color(0xFFf8b300)),
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.Center
                    ) {
                        Text(
                            text = "خرید بسته اینترنت",
                            color = Color.Black,
                            fontSize = 8.sp,
                            fontFamily = FontFamily(
                                Font(R.font.sans)
                            ),

                            )
                    }

                }
                Icon(
                    painter = painterResource(id = R.drawable.arrow),
                    contentDescription = "",
                    modifier = Modifier
                        .size(20.dp)
                        .rotate(90f)
                )

            }
        }
    }
