package com.example.myirancell.models

data class AmazingSuperMarketItem(
    val _id: String,
    val discountPercent: Int,
    val image: String,
    val name: String,
    val price: Int,
    val seller: String
)