package com.example.myirancell.navigation

import com.example.myirancell.navigation.Screen.Home.route

sealed class Screen(val route:String) {
    object Home : Screen("home_screen")
}
fun withArgs(vararg args: Any): String {
    return buildString {
        append(route)
        args.forEach {
            append("/$it")
        }
    }
}