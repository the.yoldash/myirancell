package com.example.myirancell.repository

import com.example.myirancell.models.AmazingItem
import com.example.myirancell.models.AmazingSuperMarketItem
import com.example.myirancell.remote.ApiInterface
import com.example.myirancell.remote.BaseApiResponse
import com.example.myirancell.remote.NetworkResult
import javax.inject.Inject

class HomeRepository @Inject constructor(private val apiInterface: ApiInterface):BaseApiResponse() {


    suspend fun getSuperMarketItems(): NetworkResult<List<AmazingSuperMarketItem>> =
        safeApiCall {
            apiInterface.getSuperMarketItems()
        }

}